# **What did we learn today? What activities did you do? What scenes have impressed you?**
Today, through a day of study, I learned the pyramid structure of the learning memory model, and learned some elements and points of a concept map and how to build a concept map. The main activities I participated in were to help the team members build the concept map of the software development process, and this is the scene that impressed me the most.
# **Please use one word to express your feelings about today's class**
useful
# **What do you think about this? What was the most meaningful aspect of this activity?**
The most meaningful thing is to let us think for ourselves to complete a concept map.
# **Where do you most want apply what you have learned today? What changes will you make?**
I will use concept maps when I am exposed to some new concepts, which will allow me to diverge my thinking and connect with my previous knowledge, allowing me to grasp new things more firmly.